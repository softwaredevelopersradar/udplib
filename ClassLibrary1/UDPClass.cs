﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Runtime.InteropServices;
using System.Threading;

namespace UDPLib
{
    public class UDPClass
    {
        #region Parameters
        private IPAddress remoteAddress;
        private int remotePort;
        private int localPort;
        private UdpClient SendClient;
        private UdpClient ReciveClient;
        IPEndPoint remoteIp;
        private int ttl;
        private static byte[] serialNumber = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

        private bool PortIsOpen = false;
        private byte Num = 0;

        private Thread thrRead;

        #endregion

        #region Events
        public delegate void MessageSendEventHendler(string message);
        public event MessageSendEventHendler SendEvent; //data sending event

        public delegate void MessageReciveEventHandler(object structers);
        public event MessageReciveEventHandler ReciveEvent; //data recive event

        public delegate void OpenPortEventHandler(string message);
        public event OpenPortEventHandler OpenEvent; // open port event

        public delegate void ClosePortEventHandler();
        public event ClosePortEventHandler CloseEvent; // close port event
        #endregion

        #region Properties

        public static byte[] SerialNumber
            {
            get { return serialNumber; }
            set
            {
                if (value == null || value.Length != 16)
                    return;
                serialNumber = value;
            }
        }

        public int Ttl
        {
            private get { return ttl; }
           set {
                ttl = value;
                ClosePort();
            }
        }

        public string RemoteAddress {
            get { return Convert.ToString(remoteAddress); }
            set
            {
                remoteAddress = IPAddress.Parse(value);
                ClosePort();
            }
        }

        public string RemotePort
        {
            get { return Convert.ToString(remotePort); }
            set
            {
                remotePort = Int32.Parse(value);
                ClosePort();
            }
        }

        public string LocalPort
        {
            get { return Convert.ToString(localPort); }
            set
            {
                localPort = Int32.Parse(value);
                ClosePort();
            }
        }
        #endregion

        #region Constructors
        public UDPClass() // Simple Constructor
        {

        }

        public UDPClass(string groupAddress, string remotePortForListening) // Constructor for send broadcast
        {
            try
            {
                this.remoteAddress = IPAddress.Parse(groupAddress);
                this.remotePort = Int32.Parse(remotePortForListening);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public UDPClass(string groupAddress, string localPortForSend, int ttl) // Constructor for listening broadcast
        {
            try
            {
                this.remoteAddress = IPAddress.Parse(groupAddress);
                this.localPort = Int32.Parse(localPortForSend);
                this.ttl = ttl;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public UDPClass(string groupAddress, string remotePort, string localPort, int ttl) // Constructor for Broadcast
        {
            try
            {
                this.remoteAddress = IPAddress.Parse(groupAddress);
                this.remotePort = Int32.Parse(remotePort);
                this.localPort = Int32.Parse(localPort);
                this.ttl = ttl;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        #endregion

        #region Structer
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct LocationCodegram
        {
            public int Cipher;
            public int Numcodegram;
            public int FieldLength;

            public byte[] SerialNumber;
            public byte Type;

            public double Latitude;
            public double Longtitude;
            public double Height;
            public double FlightAltitude;
        }
        #endregion

        #region Public Methods
        public void OpenPortForListening()
        {
            try
            {
                if (ReciveClient != null)
                { ReciveClient.Close(); ReciveClient = null; }

                ReciveClient = new UdpClient(localPort);
                ReciveClient.JoinMulticastGroup(remoteAddress, ttl);

                if (thrRead != null)
                {
                    thrRead.Abort();
                    thrRead = null;
                }

                thrRead = new Thread(new ThreadStart(ReceiveMessage));
                thrRead.IsBackground = true;
                thrRead.Start();

                PortIsOpen = true;
                OpenEvent?.Invoke("Port open for listening");
            }
            catch (OutOfMemoryException)
            {
                PortIsOpen = true;
                MessageBox.Show("There is not enough memory available to start read thread");
            }
            catch (Exception ex)
            {
                PortIsOpen = true;
                MessageBox.Show(ex.Message);
            }
        }

        public void OpenPortForSend()
        {
            if (PortIsOpen == true)
                ClosePort();
            try
            {
                if (SendClient != null)
                { SendClient.Close(); SendClient = null; }

                SendClient = new UdpClient();
                IPEndPoint endPoint = new IPEndPoint(remoteAddress, remotePort);
                SendClient.Connect(endPoint);

                PortIsOpen = true;
                OpenEvent?.Invoke("Port open for send");
            }
            catch (OutOfMemoryException)
            {
                PortIsOpen = true;
                MessageBox.Show("There is not enough memory available to start read thread");
            }
            catch (Exception ex)
            {
                PortIsOpen = true;
                MessageBox.Show(ex.Message);
            }
        }

        public void OpenPort()
        {
            if (PortIsOpen == true)
                ClosePort();
            try
            {     if (SendClient != null)
                  { SendClient.Close(); SendClient = null;}

                    SendClient = new UdpClient();
                    IPEndPoint endPoint = new IPEndPoint(remoteAddress, remotePort);
                    SendClient.Connect(endPoint);
                
                if (ReciveClient != null)
                  { ReciveClient.Close(); ReciveClient = null; }

                    ReciveClient = new UdpClient(localPort);
                    ReciveClient.JoinMulticastGroup(remoteAddress, ttl);

                    if (thrRead != null)
                    {
                        thrRead.Abort();
                        thrRead = null;
                    }

                    thrRead = new Thread(new ThreadStart(ReceiveMessage));
                    thrRead.IsBackground = true;
                    thrRead.Start();
                
                PortIsOpen = true;
                OpenEvent?.Invoke("Port open for send and listening");
            }
            catch (OutOfMemoryException)
            {
                PortIsOpen = true;
                MessageBox.Show("There is not enough memory available to start read thread");
            }
            catch(Exception ex)
            {
                PortIsOpen = true;
                MessageBox.Show(ex.Message);
            }           
        }

        public void ClosePort()
        {
            if (PortIsOpen == false)
                return;
            try
            {
                if (thrRead != null)
                {
                    thrRead.Abort();
                    thrRead = null;
                }

                if (SendClient != null)
                { SendClient.Close(); SendClient = null; }

                if (ReciveClient != null)
                { ReciveClient.Close(); ReciveClient = null; }

                    PortIsOpen = false;
                    CloseEvent?.Invoke();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public byte[] FormCodegramm(byte type, double Latitude, double Longtitude, float height, float FlightAltitude)
        {
            Num++;
            byte[] data = new byte[47];
            try
            {
                Array.Copy(BitConverter.GetBytes(1), 0, data, 0, 2);
                Array.Copy(BitConverter.GetBytes(Num), 0, data, 2, 2);
                Array.Copy(BitConverter.GetBytes(41), 0, data, 4, 2);
                Array.Copy(serialNumber, 0, data, 6, 16);
                data[22] = type;
                Array.Copy(BitConverter.GetBytes(Latitude), 0, data, 23, 8);
                Array.Copy(BitConverter.GetBytes(Longtitude), 0, data, 31, 8);
                Array.Copy(BitConverter.GetBytes(height), 0, data, 39, 4);
                Array.Copy(BitConverter.GetBytes(FlightAltitude), 0, data, 43, 4);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return data;
        }

        public void SendMessage(byte[] data)
        {
            try
            {               
                SendClient.Send(data, data.Length);
                SendEvent?.Invoke("Data send");
            }
            catch(NullReferenceException)
            {
                MessageBox.Show("Port open only for listening");
            }
            catch (Exception ex)
            {
              MessageBox.Show(ex.Message);
            }
        }
        #endregion


        #region Private Methods
        private void ReceiveMessage()
        {
            while (true)
            {
                byte[] data = null;

                remoteIp = null;
                try
                {              
                    data = ReciveClient.Receive(ref remoteIp);
 
                    byte[] buff = new byte[256];
                    Array.Copy(data, buff, data.Length);
                    LocationCodegram mainCodegram = new LocationCodegram();

                    mainCodegram.Cipher = BitConverter.ToInt16(CopyMassive(buff, 0, 0, 2,16) ,0);
                    mainCodegram.Numcodegram = BitConverter.ToInt16(CopyMassive(buff, 2, 0, 2,16), 0);
                    mainCodegram.FieldLength = BitConverter.ToInt16(CopyMassive(buff, 4, 0, 2,16), 0);

                    mainCodegram.SerialNumber = new byte[16];
                    Array.Copy(data, 6, mainCodegram.SerialNumber, 0, 16);
                    mainCodegram.Type = data[22];

                    mainCodegram.Latitude = BitConverter.ToDouble(CopyMassive(buff, 23, 0, 8, 8),0);
                    mainCodegram.Longtitude = BitConverter.ToDouble(CopyMassive(buff, 31, 0, 8, 8), 0);
                    mainCodegram.Height = BitConverter.ToSingle(CopyMassive(buff, 39, 0, 4,4), 0);
                    mainCodegram.FlightAltitude = BitConverter.ToSingle(CopyMassive(buff, 43, 0, 4,4), 0);

                    ReciveEvent?.Invoke(mainCodegram);
                }
                catch(ObjectDisposedException ex)
                {
                    MessageBox.Show("Recive client has been disposed /n Exception;" + ex.Message);
                }
                catch(ArgumentException ex)
                {
                    MessageBox.Show("The codogram was incorrect /n Exception;" + ex.Message);
                }
                catch (NullReferenceException)
                {
                    MessageBox.Show("Port open only for send");
                }
                catch (Exception)
                {
                    ClosePort();
                }
                Thread.Sleep(5);
            }
        }

        private byte[] CopyMassive(byte[] sourceArray, int sourceIndex, int destinationIndex, int length, int n)
        {
            byte[] newMass = new byte[length];
            Array.Copy(sourceArray, sourceIndex, newMass, destinationIndex, length);
            Array.Resize(ref newMass, n);
            return newMass;
        }
        #endregion
    }
}

