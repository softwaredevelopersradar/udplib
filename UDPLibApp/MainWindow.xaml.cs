﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using UDPLib;
using static UDPLib.UDPClass;

namespace UDPLibApp
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {  
            InitializeComponent();
            UDPClass.SerialNumber = new byte[16] {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
        }

        UDPClass firstObject;
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            firstObject.SendMessage(firstObject.FormCodegramm(1, 177, 45, 30.5f, 189f));
        }

        private void DisplayMessage(string message)
        {
            this.Dispatcher.Invoke(() =>
            {
                StandLabel.Content = message;
            });
        }

        private void ReciveEvent(object structer)
        {
            LocationCodegram mainCodegram = (LocationCodegram)structer;
            Dispatcher.Invoke(() =>
            {
                TB.Text += mainCodegram.Cipher + " \n" + mainCodegram.Numcodegram + " \n" + mainCodegram.FieldLength + " \n" + BitConverter.ToInt16(mainCodegram.SerialNumber,0) + " \n" + mainCodegram.Type
                    + " \n" + mainCodegram.Latitude + " \n" + mainCodegram.Longtitude + " \n" + mainCodegram.Height + " \n" + mainCodegram.FlightAltitude;
            }
            );
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            firstObject = new UDPClass(IPAddress.Text, localPort.Text, 20);
            firstObject.SendEvent += new UDPClass.MessageSendEventHendler(DisplayMessage);
            firstObject.ReciveEvent += new UDPClass.MessageReciveEventHandler(ReciveEvent);

            firstObject.CloseEvent += new UDPClass.ClosePortEventHandler(MessageClose);
            firstObject.OpenEvent += new UDPClass.OpenPortEventHandler(MessageOpen);
            firstObject.OpenPortForListening();
        }

        private void MessageOpen(string message)
        {
            StandLabel.Content = message;
        }        
        private void MessageClose()
        {
            StandLabel.Content = "Close port";
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            firstObject.ClosePort();
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            firstObject = new UDPClass(IPAddress.Text, remoutePort.Text);
            firstObject.SendEvent += new UDPClass.MessageSendEventHendler(DisplayMessage);
            firstObject.ReciveEvent += new UDPClass.MessageReciveEventHandler(ReciveEvent);

            firstObject.CloseEvent += new UDPClass.ClosePortEventHandler(MessageClose);
            firstObject.OpenEvent += new UDPClass.OpenPortEventHandler(MessageOpen);
            firstObject.OpenPortForSend();
        }

        private void Button_Click_4(object sender, RoutedEventArgs e)
        {
            firstObject = new UDPClass(IPAddress.Text, remoutePort.Text, localPort.Text, 20);
            firstObject.SendEvent += new UDPClass.MessageSendEventHendler(DisplayMessage);
            firstObject.ReciveEvent += new UDPClass.MessageReciveEventHandler(ReciveEvent);

            firstObject.CloseEvent += new UDPClass.ClosePortEventHandler(MessageClose);
            firstObject.OpenEvent += new UDPClass.OpenPortEventHandler(MessageOpen);
            firstObject.OpenPort();
        }
    }
}
